create sequence hibernate_sequence start 1 increment 1;

create table bot (
                     id int8 not null,
                     experience int4 not null,
                     img_url varchar(255),
                     level int4 not null,
                     name varchar(255),
                     primary key (id)
);

create table character (
                           id int8 not null,
                           experience int4 not null,
                           img_url varchar(255),
                           level int4 not null,
                           name varchar(255),
                           primary key (id)
);

create table config (
                        level int4 not null,
                        level_up int4,
                        primary key (level)
);

create table user_role (
                           user_id int8 not null,
                           roles varchar(255)
);

create table usr (
                     id int8 not null,
                     active boolean not null,
                     confirm_password varchar(255),
                     password varchar(255),
                     username varchar(255),
                     character_id int8,
                     primary key (id)
);

alter table if exists user_role
    add constraint user_role_user_fk
    foreign key (user_id) references usr;
alter table if exists usr
    add constraint usr_character_fk
    foreign key (character_id) references character;

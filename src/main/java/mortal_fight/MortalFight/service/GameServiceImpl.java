package mortal_fight.MortalFight.service;

import mortal_fight.MortalFight.dto.UserDetailsResponse;

import mortal_fight.MortalFight.entity.Bot;
import mortal_fight.MortalFight.entity.Character;
import mortal_fight.MortalFight.entity.User;
import mortal_fight.MortalFight.repository.BotRepository;
import mortal_fight.MortalFight.repository.CharacterRepository;
import mortal_fight.MortalFight.repository.ConfigRepository;
import mortal_fight.MortalFight.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Random;


@Service
public class GameServiceImpl implements GameService {

    @Autowired
    private UserRepository uRepo;

    @Autowired
    private CharacterRepository cRepo;

    @Autowired
    private ConfigRepository confRepo;

    @Autowired
    private BotRepository botRepo;


    public UserDetailsResponse getUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        String name = userDetails.getUsername();
        return uRepo.getJoinInformation(name);
    }


    public Bot createBot(UserDetailsResponse real) {

        Random random = new Random();
        Bot newBot = botRepo.findById((long) (random.nextInt(10 ) + 1)).orElse(new Bot());
        newBot.setLevel(real.getLevel());
        newBot.setExperience(real.getExperience() + (int) (Math.random() * 10));


        return newBot;
    }

    public int getWinner(UserDetailsResponse realUser) {
        if (isWin()) {
            return 10;
        } else {
            return 0;
        }
    }


    public boolean isWin() {
        int realScore = 0;
        int botScore = 0;
        int win = 100;

        for (int i = 0; i <= 2; i++) {
            realScore += (int) (Math.random() * 10);
            botScore += (int) (Math.random() * 10);
        }
        if (win - realScore < win - botScore) {
            return true;
        } else {
            return false;
        }
    }

    public void update(UserDetailsResponse realUser, int exp) {

        Integer newExp = realUser.getExperience() + exp;
        int lvlFromDb = realUser.getLevel();

        Integer lvlUpExp = confRepo.findByLevel(lvlFromDb).getLevelUp();


        if (newExp < lvlUpExp) {
            User user = uRepo.findByUsername(realUser.getUName());
            Character character = cRepo.findCharacterById(user.getCharacter().getId());
            character.setExperience(newExp);
            cRepo.save(character);
        } else {
            int finalExp = newExp - (int) lvlUpExp;
            int finalLvl = lvlFromDb + 1;
            User user = uRepo.findByUsername(realUser.getUName());
            Character character = cRepo.findCharacterById(user.getCharacter().getId());
            character.setExperience(finalExp);
            character.setLevel(finalLvl);
            cRepo.save(character);
        }

    }

    public void updateCharacterName(String newName, UserDetailsResponse realUser) {
        String userName=realUser.getUName();
        User user=uRepo.findByUsername(userName);
        user.getCharacter().setName(newName);
        uRepo.save(user);

    }
}

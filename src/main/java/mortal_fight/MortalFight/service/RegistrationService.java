package mortal_fight.MortalFight.service;

import mortal_fight.MortalFight.entity.User;

public interface RegistrationService {
    String addUser(User user);
}

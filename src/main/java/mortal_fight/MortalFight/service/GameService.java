package mortal_fight.MortalFight.service;

import mortal_fight.MortalFight.dto.UserDetailsResponse;
import mortal_fight.MortalFight.entity.Bot;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public interface GameService {
    UserDetailsResponse getUserDetails();

    Bot createBot(UserDetailsResponse real);

    int getWinner(UserDetailsResponse realUser);

    void update(UserDetailsResponse realUser, int exp) throws IOException, ParseException;

    void updateCharacterName(String newName, UserDetailsResponse realUser);
}


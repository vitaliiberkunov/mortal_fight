package mortal_fight.MortalFight.service;

import mortal_fight.MortalFight.entity.Character;
import mortal_fight.MortalFight.entity.Role;
import mortal_fight.MortalFight.entity.User;
import mortal_fight.MortalFight.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    private UserRepository userRepo;

    public String addUser(User user) {
        if (user.getPassword().equals(user.getConfirmPassword())) {
            User userFromDb = userRepo.findByUsername(user.getUsername());
            if (userFromDb != null) {
                return "entrance";
            }
            user.setActive(true);
            user.setRoles(Collections.singleton(Role.USER));
            user.setCharacter(new Character());
            userRepo.save(user);
            return "redirect:/entrance";
        }

        return "registration";
    }

}

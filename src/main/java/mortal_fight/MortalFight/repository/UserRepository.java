package mortal_fight.MortalFight.repository;

import mortal_fight.MortalFight.dto.UserDetailsResponse;
import mortal_fight.MortalFight.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    @Query("SELECT new mortal_fight.MortalFight.dto.UserDetailsResponse(u.username, c.name, c.level,c.experience,c.imgUrl) FROM User u JOIN u.character c WHERE u.username = :name")
    UserDetailsResponse getJoinInformation(String name);

}
package mortal_fight.MortalFight.repository;

import mortal_fight.MortalFight.entity.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    Character findCharacterById(Long id);
}

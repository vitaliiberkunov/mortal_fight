package mortal_fight.MortalFight.repository;

import mortal_fight.MortalFight.entity.Config;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigRepository extends JpaRepository<Config, Integer> {
    Config findByLevel(int level);
}

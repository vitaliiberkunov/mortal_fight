package mortal_fight.MortalFight.repository;

import mortal_fight.MortalFight.entity.Bot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotRepository extends JpaRepository<Bot,Long> {
}

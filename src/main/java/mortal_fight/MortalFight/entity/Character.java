package mortal_fight.MortalFight.entity;

import lombok.*;

import javax.persistence.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "character")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name = "Kukuha Makuha";
    private String imgUrl = "man.png";
    private int experience = 0;
    private int level = 1;
    @OneToOne(mappedBy = "character", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private User user;
}

package mortal_fight.MortalFight.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDetailsResponse {
    private String uName;
    private String cName;
    private int level;
    private int experience;
    private String imgUrl;
}

package mortal_fight.MortalFight.controller;

import mortal_fight.MortalFight.entity.User;
import mortal_fight.MortalFight.service.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class RegistrationController {

    @Autowired
    private RegistrationServiceImpl service;

    @GetMapping("/first")
    public String first() {
        return "first";
    }

    @GetMapping("/entrance")
    public String entrancePage() {
        return "entrance";
    }


    @GetMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user) {
        return service.addUser(user);
    }

}


package mortal_fight.MortalFight.controller;

import mortal_fight.MortalFight.dto.UserDetailsResponse;
import mortal_fight.MortalFight.entity.Bot;
import mortal_fight.MortalFight.entity.Character;
import mortal_fight.MortalFight.entity.User;
import mortal_fight.MortalFight.service.GameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class GameController {
    @Autowired
    private GameServiceImpl service;


    @GetMapping("/home")
    public String goToUserPage(Model model) {

        UserDetailsResponse info = service.getUserDetails();
        model.addAttribute("uName", info.getUName());
        model.addAttribute("cName", info.getCName());
        model.addAttribute("level", info.getLevel());
        model.addAttribute("experience", info.getExperience());
        model.addAttribute("imgUrl", info.getImgUrl());

        return "home";
    }

    @GetMapping("/start")
    public String start(Model model) {

        UserDetailsResponse realUser = service.getUserDetails();
        model.addAttribute("r_u_name", realUser.getUName());
        model.addAttribute("r_c_name", realUser.getCName());
        model.addAttribute("r_level", realUser.getLevel());
        model.addAttribute("r_experience", realUser.getExperience());
        model.addAttribute("r_imgUrl", realUser.getImgUrl());

        Bot bot = service.createBot(realUser);
        model.addAttribute("b_name", bot.getName());
        model.addAttribute("b_level", bot.getLevel());
        model.addAttribute("b_experience", bot.getExperience());
        model.addAttribute("b_imgUrl", bot.getImgUrl());


        return "start";
    }

    @GetMapping("/fight")
    public String buttle(Model model) {
        UserDetailsResponse realUser = service.getUserDetails();
        int exp = service.getWinner(realUser);
        service.update(realUser, exp);
        model.addAttribute("r_u_name", realUser.getUName());
        model.addAttribute("r_c_name", realUser.getCName());
        model.addAttribute("r_level", realUser.getLevel());
        model.addAttribute("r_experience", realUser.getExperience());
        model.addAttribute("r_imgUrl", realUser.getImgUrl());
        model.addAttribute("exp",exp);
        if(exp>0) {
            model.addAttribute("txt", "You WIN! ;)");
        }else{
            model.addAttribute("txt", "You LOSE :( ");
        }
        Bot bot = service.createBot(realUser);
        model.addAttribute("b_name", bot.getName());
        model.addAttribute("b_level", bot.getLevel());
        model.addAttribute("b_experience", bot.getExperience());
        model.addAttribute("b_imgUrl", bot.getImgUrl());
        return "start";
    }

    @PostMapping("/update")
    public String updateUserName(@RequestParam("new_name") String newName, Model model){
        UserDetailsResponse realUser = service.getUserDetails();
        service.updateCharacterName(newName,realUser);
        model.addAttribute("uName", realUser.getUName());
        model.addAttribute("cName", realUser.getCName());
        model.addAttribute("level", realUser.getLevel());
        model.addAttribute("experience", realUser.getExperience());
        model.addAttribute("imgUrl", realUser.getImgUrl());
        return "redirect:home";
    }
}
